<!DOCTYPE html>
@section('html-tag')
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
@show
	<head>
		@section('head')
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">

			<title>{{ $title or trans('vi::cms.name') }}</title>

			@section('meta-tags')
				<?php $metatags = isset($metatags) ? $metatags : [] ; ?>
				<?php $metatags = array_merge( [
					'viewport' => 'width=device-width, initial-scale=1',
					'title' => isset($title) ? $title : trans('vi::cms.name'),
					'description' => '',
				], $metatags ) ?>

				@foreach($metatags as $name => $content)
					<meta name="{{ $name }}" content="{{ $content }}">
				@endforeach
			@show

			@section('css')
				<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.5.0/pure-min.css">
			@show

			@section('js-head')
				<script src="js/vendor/modernizr-2.6.2.min.js"></script>
			@show
		@show
	</head>
	@section('body-tag')
		<body>
	@show
		@yield('outdated')
			<!--[if lt IE 9]>
				<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
			<![endif]-->
		@show

		@yield('body')

		@section('js')
		@show
	</body>
</html>
