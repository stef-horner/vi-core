@extend('vi::layout')

@section('body')

	<div id="layout" class="pure-g">
	    <div class="sidebar pure-u-1 pure-u-md-1-4">
	        <div class="header">
	            <h1 class="brand-title">
	            	<a href="">
						User Information
	            	</a>
	            </h1>
	            <h2 class="brand-tagline">top level options - logout, etc.</h2>

	            <nav class="nav">
	                <ul class="nav-list">
	                    <li class="nav-item">
	                        <a class="pure-button" href="">Home</a>
	                    </li>
	                    <li class="nav-item">
	                        <a class="pure-button" href="">Logout</a>
	                    </li>
	                </ul>
	            </nav>
	        </div>
	    </div>

	    <div class="content pure-u-1 pure-u-md-3-4">
			<div>breadcrumb / to / here - parse route annotations for breadcrumbs?</div>

			{{ $title or trans('vi::cms.name') }}

			<div class="content">
				@yield('content')
			</div>
	    </div>
	</div>

@stop
