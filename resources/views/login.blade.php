@extend('vi::layout')

@section('body')

	<div class="splash-container">
		<div class="splash">
			<form action="" class="pure-form pure-form-aligned">
				<h1 class="splash-head">Login</h1>
				<p class="splash-subhead">
					<fieldset>
						<div class="pure-control-group">
							<label for="name">Username</label>
							<input id="username" type="text" placeholder="Username">
						</div>

						<div class="pure-control-group">
							<label for="password">Password</label>
							<input id="password" type="password" placeholder="Password">
						</div>
					</fieldset>
				</p>
				<p>
					<input type="submit" href="http://purecss.io" class="pure-button pure-button-primary" value="Log in">
					<a href="">Forgot Password</a>
					<a href="">Register</a>
				</p>
			</form>
		</div>
	</div>

@stop
