<?php namespace Tlr\Support\Eloquent\Scopes;

use Illuminate\Database\Eloquent\Builder;

trait PermissionsTrait {

	/**
	 * The available permissions for the model
	 * @var array
	 */
	// public static $permissionsList = [];

	/**
	 * Grant the given permissions to the model
	 * @param  array|string $permissions
	 * @return array
	 */
	public function grant( $permissionsToGrant )
	{
		$permissionsToGrant = is_array($permissionsToGrant) ? $permissionsToGrant : func_get_args();

		$currentPermissions = $this->permissions;

		foreach ($permissionsToGrant as $permission)
		{
			$currentPermissions[] = $permission;
		}

		return $this->permissions = $currentPermissions;
	}

	/**
	 * Deny the given permissions from the model
	 * @param  array|string $permissionsToDeny
	 * @return array
	 */
	public function deny( $permissionsToDeny )
	{
		$permissionsToDeny = is_array($permissionsToDeny) ? $permissionsToDeny : func_get_args();

		$currentPermissions = $this->permissions;

		foreach ($permissionsToDeny as $permission)
		{
			$index = array_search($permission, $currentPermissions);

			if ($index !== false)
			{
				array_splice($currentPermissions, $index, 1);
			}
		}

		return $this->permissions = $currentPermissions;
	}

	/**
	 * Check if all of the given permissions are present
	 * @param  array|string $permissions
	 * @return boolean
	 */
	public function can( $permissionsToCheck )
	{
		$permissionsToCheck = is_array($permissionsToCheck) ? $permissionsToCheck : func_get_args();

		$currentPermissions = $this->permissions;

		foreach ($permissionsToCheck as $permission)
		{
			if ( ! in_array($permission, $currentPermissions) )
			{
				return false;
			}
		}

		return true;
	}

	/**
	 * Check if any of the given permissions are present
	 * @param  array|string $permissions
	 * @return boolean
	 */
	public function canDoAny( $permissionsToCheck )
	{
		$permissionsToCheck = is_array($permissionsToCheck) ? $permissionsToCheck : func_get_args();

		$currentPermissions = $this->permissions;

		foreach ($permissionsToCheck as $permission)
		{
			if ( in_array($permission, $currentPermissions) )
			{
				return true;
			}
		}

		return false;
	}

	/**
	 * Apply a where can query scope to the model
	 *
	 * @param  \Illuminate\Database\Eloquent\Builder     $query
	 * @param  array|string                              $permissions
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeWhereCan(Builder $query, $permissions)
	{
		foreach ((array) $permissions as $permission)
		{
			$query->where( $this->getPermissionsKey(), 'LIKE', "%\"{$permission}\"%" );
		}

		return $query;
	}

	/**
	 * Apply an 'or' style where can query scope to the model
	 *
	 * @param  \Illuminate\Database\Eloquent\Builder     $query
	 * @param  array|string                              $permissions
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeWhereCanDoAny(Builder $query, $permissions)
	{
		$permissions = (array) $permissions;

		if ( empty($permissions) ) return $query;

		$query->whereNested(function($query) use ($permissions)
		{
			$query->where( $this->getPermissionsKey(), 'LIKE', '%"' . array_shift($permissions) . '"%' );

			foreach ( $permissions as $permission )
			{
				$query->orWhere( $this->getPermissionsKey(), 'LIKE', "%\"{$permission}\"%" );
			}
		});


		return $query;
	}

	/**
	 * Convert the permissions to an array
	 * @return array
	 */
	public function getPermissionsAttribute()
	{
		return json_decode( $this->attributes[ $this->getPermissionsKey() ] );
	}

	/**
	 * Convert the given permissions to a json string for storage
	 * @param array|string $permissions
	 */
	public function setPermissionsAttribute( $permissions )
	{
		$finalPermissions = [];

		foreach ( (array) $permissions as $permission )
		{
			if ( in_array($permission, static::$permissionsList) )
			{
				$finalPermissions[] = $permission;
			}
		}

		natsort($finalPermissions);

		$this->attributes[ $this->getPermissionsKey() ] = json_encode(
			array_values( array_unique($finalPermissions) )
		);
	}

	/**
	 * Get the key used to store permissions
	 * @return string
	 */
	public function getPermissionsKey()
	{
		return 'permissions';
	}

}
