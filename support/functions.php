<?php

if ( ! function_exists('array_find_dot'))
{
	/**
	 * Find an item of an array by a dot notation accessible property
	 *
	 * @param  string  $needle
	 * @param  array   $haystack
	 * @param  string  $key
	 * @param  mixed   $default
	 * @return string
	 */
	function array_find_dot( $needle, array $haystack, $location, $default = null, $returnItem = true )
	{
		foreach ($haystack as $key => $value)
		{
			if ( dot_get( $value, $location ) == $needle )
			{
				return $returnItem ? $value : $key;
			}
		}
		return $default;
	}
}

if ( ! function_exists('array_splice_item'))
{
	/**
	 * Find an item of an array by a dot notation accessible property
	 *
	 * @param  string  $needle
	 * @param  array   $haystack
	 * @param  string  $key
	 * @param  mixed   $default
	 * @return string
	 */
	function array_splice_item( array &$haystack, $item, $default = null )
	{
		if ( ($key = array_search($item, $haystack)) === false)
		{
			return null;
		}
		return array_splice($haystack, $key, 1)[0];
	}
}

if ( ! function_exists('class_dirname'))
{
	/**
	 * Get the class "dirname" of the given object / class (ie. the parent namespace)
	 *
	 * @param  string|object  $class     The object or class to dirname
	 * @param  integer        $levels    How many levels to dirname (default: 1)
	 * @param  boolean        $basename  Whether or not to basename the dirname (default: false)
	 * @return string
	 */
	function class_dirname($class, $levels = 1, $basename = false)
	{
		$class = is_object($class) ? get_class($class) : $class;
		$classpath = str_replace('\\', '/', $class);
		for ( $xi = 0; $xi < $levels; $xi++ )
		{
			$classpath = dirname($classpath);
		}
		$classpath = str_replace('/', '\\', $classpath);
		return $basename ? class_basename( $classpath ) : $classpath;
	}
}

if ( ! function_exists('path_compile'))
{
	/**
	 * Put a bunch of path components together
	 *
	 * @param  string|array  $paths
	 * @return string
	 */
	function path_compile( $paths = '' )
	{
		$paths = is_array($paths) ? $paths : func_get_args();
		foreach ($paths as $xi => &$component)
		{
			$component = ($xi == 0) ? rtrim($component, '/') : trim($component, '/');
		}
		return implode('/', $paths);
	}
}

if ( ! function_exists('data_assign'))
{
	/**
	 * Assign the given data to the target
	 * @param  object        $model
	 * @param  array         $data
	 * @param  array|string  $map
	 * @return \Illuminate\Database\Eloquent\Model
	 */
	function data_assign( $target, array $data, $map )
	{
		// Get microtime as a safe unique value
		$unique = microtime();

		foreach ( (array) $map as $to => $from )
		{
			// if the element is not associative, map to the same key
			if ( is_int($to) )
				$to = $from;

			// if the value exists in the data, assign it to the target
			if ( ($value = array_get( $data, $from, $unique )) !== $unique )
				$target->{$to} = $value;
		}

		return $target;
	}
}
