<?php namespace Velox\Content;

use Illuminate\Database\Eloquent\Model;

use Velox\Users\User;

class Content extends Model {

	$fillable = [ 'name' ];

	/**
	 * The page's editor
	 * @return BelongsTo
	 */
	public function author()
	{
		return $this->belongsTo( User::class, 'author_id' );
	}

	/**
	 * The content's type
	 * @return MorphTo
	 */
	public function type()
	{
		return $this->morphTo();
	}

}
