<?php namespace Velox\Content;

use Illuminate\Database\Eloquent\Model;

abstract class ContentType extends Model {

	/**
	 * The content's type
	 * @return MorphTo
	 */
	public function parent()
	{
		return $this->morphToMany( Content::class, 'type' );
	}

}
