<?php namespace Velox\Contents;

class ContentRepository {

	/**
	 * Make and save a new content model
	 * @param  array  $data
	 * @return Content
	 */
	public function create( array $data )
	{
		$content = new Content( $data );

		data_assign( $content, $data, [
			'author' => 'author_id'
		] );

		$content->save();

		return $content;
	}

	/**
	 * Edit the given content model
	 * @param  array  $data
	 * @return Content
	 */
	public function edit( Content $content, array $data )
	{
		$content->fill( $data );

		data_assign( $content, $data, [
			'author' => 'author_id'
		] );

		$content->save();

		return $content;
	}

	/**
	 * Delete a content model
	 * @param  array  $data
	 * @return Content
	 */
	public function delete( Content $content )
	{
		$content->delete();
	}

}
