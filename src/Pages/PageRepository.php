<?php namespace Velox\Pages;

class PageRepository {

	/**
	 * Make and save a new page model
	 * @param  array  $data
	 * @return Page
	 */
	public function create( array $data )
	{
		$page = new Page( $data );

		data_assign( $page, $data, [
			'author' => 'author_id'
		] );

		$page->save();

		return $page;
	}

	/**
	 * Edit the given page model
	 * @param  array  $data
	 * @return Page
	 */
	public function edit( Page $page, array $data )
	{
		$page->fill( $data );

		data_assign( $page, $data, [
			'author' => 'author_id'
		] );

		$page->save();

		return $page;
	}

	/**
	 * Delete a page model
	 * @param  array  $data
	 * @return Page
	 */
	public function delete( Page $page )
	{
		$page->delete();
	}

}
