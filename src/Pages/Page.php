<?php namespace Velox\Pages;

use Illuminate\Database\Eloquent\Model;

use Velox\Users\User;

class Page extends Model {

	$fillable = [ 'name', 'body' ];

	/**
	 * The page's editor
	 * @return BelongsTo
	 */
	public function author()
	{
		return $this->belongsTo( User::class, 'author_id' );
	}

}
