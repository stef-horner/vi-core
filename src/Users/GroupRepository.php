<?php namespace Velox\Users;

class GroupRepository {

	/**
	 * Make and save a new group model
	 * @param  array  $data
	 * @return Group
	 */
	public function create( array $data )
	{
		return Group::create( $data );
	}

	/**
	 * Edit the given group model
	 * @param  array  $data
	 * @return Group
	 */
	public function edit( Group $group, array $data )
	{
		$group->fill( $data );

		$group->save();

		return $group;
	}

	/**
	 * Delete a group model
	 * @param  array  $data
	 * @return Group
	 */
	public function delete( Group $group )
	{
		$group->delete();
	}

}
