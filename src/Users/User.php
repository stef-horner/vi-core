<?php namespace VI\Users;

use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;

use Tlr\Support\Auth\UserTrait;

class User extends Model implements AuthenticatableContract {

	use UserTrait;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	/**
	 * A relationship to the user's groups
	 * @return BelongsToMany
	 */
	public function groups()
	{
		return $this->belongsToMany( Group::class );
	}

}
