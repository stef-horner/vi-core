<?php namespace Velox\Users;

use Illuminate\Database\Eloquent\Model;

class Group extends Model {

	$fillable = [ 'name' ];

	/**
	 * A relationship to the group's users
	 * @return BelongsToMany
	 */
	public function users()
	{
		return $this->belongsToMany( User::class );
	}

}
