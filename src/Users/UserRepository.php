<?php namespace Velox\Users;

class UserRepository {

	/**
	 * Make and save a new user model
	 * @param  array  $data
	 * @return User
	 */
	public function create( array $data )
	{
		return User::create( $data );
	}

	/**
	 * Edit the given user model
	 * @param  array  $data
	 * @return User
	 */
	public function edit( User $user, array $data )
	{
		$user->fill( $data );

		$user->save();

		return $user;
	}

	/**
	 * Delete a user model
	 * @param  array  $data
	 * @return User
	 */
	public function delete( User $user )
	{
		$user->delete();
	}

}
