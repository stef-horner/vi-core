REPORT_DIR=./report
SUITE=unit

clean:
	@rm -rf vendor
	@rm -rf tmp

test:
	@./vendor/bin/phpunit -v --testsuite "$(SUITE)"

coverage:
	@./vendor/bin/phpunit --coverage-html $(REPORT_DIR)

report:
	@open $(REPORT_DIR)/index.html

coverage-report: coverage report
