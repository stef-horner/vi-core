#! /usr/bin/env bash

VI_LARAVEL_VERSION='dev-develop'

echo 'Enter project name:'

read VI_PROJECT_NAME < /dev/tty

echo "Creating ${VI_PROJECT_NAME} folder"

echo "Downloading and Installing Laravel... (may take a couple of minutes)"

composer create-project "laravel/laravel:${VI_LARAVEL_VERSION}" ${VI_PROJECT_NAME} --prefer-dist

cd ${VI_PROJECT_NAME}

exit

echo "Adding VI"

# composer require tlr/vi-core "dev-master"

mkdir -p workbench/tlr
git clone git@bitbucket.org:stef-horner/vi-core.git workbench/tlr/vi-core >& /dev/null

pushd workbench/tlr/vi-core >& /dev/null

echo "Installing VI dependancies"
composer install --prefer-dist --quiet

popd >& /dev/null

echo "Installing Laravel dependancies"
composer install --prefer-dist --quiet

exit
