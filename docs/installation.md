VI: Installation
================

Currently, the project is in development. To install for development, run `curl https://bitbucket.org/stef-horner/vi-core/raw/master/install.sh | bash`, and choose a project name. This should create a clean laravel project, and add this package to its workbench.

It will, in future, attempt to publish resources, and make any necessary changes to the default project structure to get the project to run.

This is a temporary script.
